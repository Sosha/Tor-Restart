#!/bin/bash 

commands () {
echo "Tor is trying to establish a connection. This may take long for some minutes. Please wait" | sudo tee /var/log/tor/log
bootstraped='n'
sudo systemctl restart tor.service
while [ $bootstraped == 'n' ]; do
 	if sudo cat /var/log/tor/log | grep "Bootstrapped 100%: Done"; then
		bootstraped='y'
        notify-send -u critical -i ~/Pictures/400px-Warning_icon.svg "Tor Restarted!"
        exit
	else
		sleep 1
	fi
done
}

export -f commands

gnome-terminal -e "bash -c 'commands'"
